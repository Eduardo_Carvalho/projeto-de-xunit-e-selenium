﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using Alura.LeilaoOnline.Selenium.Helpers;
using Xunit;
using Alura.LeilaoOnline.Selenium.PageObjects;

namespace Alura.LeilaoOnline.Selenium.Testes
{
    public class AoNavegarParaHomeMobile : IDisposable
    {
        private ChromeDriver driver;

        public AoNavegarParaHomeMobile()
        {
            
        }

        [Fact]
        public void DadaLargura992DeveMostrarMenuMobile()
        {
            //arrange
            var deviceSettings = new ChromeMobileEmulationDeviceSettings();
            deviceSettings.Width = 992;
            deviceSettings.Height = 800;
            deviceSettings.UserAgent = "customiada";
            var options = new ChromeOptions();
            options.EnableMobileEmulation(deviceSettings);
            driver = new ChromeDriver(TestHelper.PastaDoExecutavel, options);

            var HomePO = new HomeNaoLogadaPO(driver);
            //act
            HomePO.Visitar();
            //assert
            Assert.True(HomePO.menu.MenuMobileVisible);
        }
        [Fact]
        public void DadaLargura993NaoDeveMostrarMenuMobile()
        {
            //arrange
            var deviceSettings = new ChromeMobileEmulationDeviceSettings();
            deviceSettings.Width = 993;
            deviceSettings.Height = 800;
            deviceSettings.UserAgent = "customiada";
            var options = new ChromeOptions();
            options.EnableMobileEmulation(deviceSettings);
            driver = new ChromeDriver(TestHelper.PastaDoExecutavel, options);

            var HomePO = new HomeNaoLogadaPO(driver);
            //act
            HomePO.Visitar();
            //assert
            Assert.False(HomePO.menu.MenuMobileVisible);
        }
        public void Dispose()
        {
            driver.Quit();
        }
    }
}
