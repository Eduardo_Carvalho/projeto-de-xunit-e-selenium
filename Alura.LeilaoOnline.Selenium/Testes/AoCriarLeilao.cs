﻿using OpenQA.Selenium;
using System;
using Xunit;
using Alura.LeilaoOnline.Selenium.PageObjects;
using Alura.LeilaoOnline.Selenium.Fixtures;
using System.Threading;

namespace Alura.LeilaoOnline.Selenium.Testes
{
    [Collection("Chrome Driver")]
    public class AoCriarLeilao
    {
        private IWebDriver driver;

        public AoCriarLeilao (TestFixture fixture)
        {
            driver = fixture.Driver;
        }
        [Fact]
        public void DadoLoginAdminEInfoValidosDeveCadastrarLeilao()
        {
            //arrange
            var LoginPO = new LoginPO(driver);
            LoginPO.Visitar();
            LoginPO.PreencheFormulario("admin@example.org", "123");
            LoginPO.SubmeteFormulario();

            var NovoLeilaoPO = new NovoLeilaoPO(driver);
            NovoLeilaoPO.Visitar();
            NovoLeilaoPO.PreencheFormulario(
                "Leilão de coleção 1",
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce venenatis dolor eu dui venenatis mattis.",
                "Item de Colecionador",
                4000,
                "C:\\Users\\Knalha\\Pictures\\download.jpg",
                DateTime.Now.AddDays(20),
                DateTime.Now.AddDays(40)
                );

                Thread.Sleep(4000);

            //act
            NovoLeilaoPO.SubmeteFormulario();
            //assert
            Assert.Contains("Leilões cadastrados no sistema", driver.PageSource);
        }


    }


}
