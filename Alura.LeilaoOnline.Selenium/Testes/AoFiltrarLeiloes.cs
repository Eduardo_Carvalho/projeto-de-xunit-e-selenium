﻿using OpenQA.Selenium;
using Xunit;
using Alura.LeilaoOnline.Selenium.Fixtures;
using Alura.LeilaoOnline.Selenium.PageObjects;
using System.Collections.Generic;
using System.Threading;

namespace Alura.LeilaoOnline.Selenium.Testes
{
    [Collection("Chrome Driver")]
    public class AoFiltrarLeiloes
    {
        private IWebDriver driver;

        public AoFiltrarLeiloes(TestFixture fixture)
        {
            driver = fixture.Driver;
        }

        [Fact]
        public void DadoLoginValidoDeveMostrarPainelDeResultado()
        {
            //arrange
            new LoginPO(driver)
                .AoEfetuarLoginComCredenciais("fulano@example.org", "123");
            
            var DashboardPO = new DashboardPO(driver);
            //act
            DashboardPO.Filtro.PesquisarLeiloes(new List<string> { "Arte", "Coleções" }
                , "",
                true);

            //assert
            Assert.Contains("Resultado da pesquisa", driver.PageSource);
            Thread.Sleep(3000);
        }
    }
}
