﻿using OpenQA.Selenium;

namespace Alura.LeilaoOnline.Selenium.PageObjects
{
    public class MenuNaoLogadoPO
    {
        private IWebDriver driver;
        private By ByMenuMobile;
        public bool MenuMobileVisible 
        {
            get
            {
                var elemento = driver.FindElement(ByMenuMobile);
                return elemento.Displayed;
            }
        
        
        }
        public MenuNaoLogadoPO(IWebDriver driver)
        {
            this.driver = driver;
            ByMenuMobile = By.ClassName("sidenav-trigger");
        }
    }
}