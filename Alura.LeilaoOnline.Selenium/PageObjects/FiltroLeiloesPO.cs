﻿using Alura.LeilaoOnline.Selenium.Helpers;
using OpenQA.Selenium;
using System.Collections.Generic;
using System.Threading;

namespace Alura.LeilaoOnline.Selenium.PageObjects
{
    public class FiltroLeiloesPO
    {
        private By BySelectCategorias;
        private By ByInputTermo;
        private By ByInputAndamento;
        private By ByBotaoPesquisar;

        private IWebDriver driver;

        public FiltroLeiloesPO(IWebDriver driver)
        {
            this.driver = driver;
            BySelectCategorias = By.ClassName("select-wrapper");
            ByInputTermo = By.Id("termo");
            ByInputAndamento = By.ClassName("lever");
            ByBotaoPesquisar = By.CssSelector("form>button.btn");
        }
        public void PesquisarLeiloes(
            List<string> categorias,
            string termo,
            bool emAndamento)
        {
            var select = new SelectMaterialize(driver, BySelectCategorias);
            //var selectWrapper = driver.FindElement(BySelectCategorias);
            //selectWrapper.Click();

            //var opcoes = driver
            //    .FindElements(By.CssSelector("li>span"))
            //    .ToList();
            Thread.Sleep(1000);
            select.DeselectAll();
            //opcoes.ForEach(o =>
            //{
            //    o.Click();
            //});
            Thread.Sleep(1000);
            categorias.ForEach(categ =>
            {
                select.SelectByText(categ);
                // opcoes
                // .Where(o => o.Text.Contains(categ))
                // .ToList()
                // .ForEach(o =>
                //{
                //    o.Click();
                //});
            });

            //selectWrapper
            //    .FindElement(By.TagName("li"))
            //    .SendKeys(Keys.Tab);

            driver.FindElement(ByInputTermo).SendKeys(termo);
            Thread.Sleep(1000);
            if (emAndamento == true)
            {
                driver.FindElement(ByInputAndamento).Click();
            }
            Thread.Sleep(3000);
            driver.FindElement(ByBotaoPesquisar).Click();
        }

    }
}
