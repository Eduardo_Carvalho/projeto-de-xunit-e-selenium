Projeto final do curso de Selenium da Alura
Projeto criado usando:
-Visual Studio 2019
-.NetCore 2.2
-xUnit
-Selenium Webdriver

Para inicializar o projeto:
-Defina o projeto Alura.LeilaoOnline.WebApp (IIS Express) como projeto de inicialização;
-Realize o build do projeto (ctrl+shift+B);
-Abra o console de gerenciador de pacotes NuGet > projeto padrão "Alura.LeilaoOnline.WebApp" > Insira o comando  "Update-Database";
-Verificar no "SQL Server Object Explorer" se o BD foi criado;
